\connect "chaipiK0"

CREATE TABLE "users" (
  "id" serial PRIMARY KEY,
  "username" character varying(200) UNIQUE NOT NULL,
  "email" character varying(200) UNIQUE NOT NULL,
  "profile_picture_url" character varying(500),  
  "password" character varying(200) NOT NULL,
  "auth_type" character varying(20),
  "is_online" boolean NOT NULL DEFAULT FALSE,
  "is_verified" boolean NOT NULL DEFAULT FALSE,
  "verification_code" character varying(30),
  "verification_sent_at" timestamp without time zone,
  "password_reset_code" character varying(30),
  "password_reset_sent_at" timestamp without time zone
);

CREATE TABLE "channels" (
  "id" serial PRIMARY KEY,
  "user" integer REFERENCES users ON DELETE CASCADE,
  "name" character varying(50) NOT NULL,
  "type" character varying(20) NOT NULL,
  "configuration" jsonb NOT NULL,
  "reminder_time" time NOT NULL,
  "reminder_timezone" text NOT NULL,
  "reminder_days_before" integer[],
  UNIQUE ("user", "name")
);